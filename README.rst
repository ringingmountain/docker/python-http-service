Python HTTP Service
===================

HTTP service base image based on Python 3, Tornado 6, and sprockets.http.

+---------------+-------------------------------------------------------------------------+
| Repository    | https://gitlab.com/ringingmountain/docker/python-http-service           |
+---------------+-------------------------------------------------------------------------+
| CI Pipeline   | https://gitlab.com/ringingmountain/docker/python-http-service/pipelines |
+---------------+-------------------------------------------------------------------------+
| Documentation | https://docs.ringingmountain.com/python-http-service                    |
+---------------+-------------------------------------------------------------------------+

|pipeline|


Usage
-----

.. code-block:: bash

   docker run registry.gitlab.com/ringingmountain/docker/python-http-service:latest


Quickstart Development Guide
----------------------------

Build Documentation
~~~~~~~~~~~~~~~~~~~

.. code-block:: bash

   ci/docs.sh


Build Docker Image
~~~~~~~~~~~~~~~~~~

.. code-block:: bash

  docker build -t registry.gitlab.com/ringingmountain/docker/python-http-service:local .


Run Gitlab CI Jobs Locally
~~~~~~~~~~~~~~~~~~~~~~~~~~

You will first need to install the `Gitlab runner`_ package and `register`_ a runner on your local machine.

.. code-block:: bash

   gitlab-runner exec docker <job_name> --docker-services docker:dind --docker-privileged


Releasing a New Version
~~~~~~~~~~~~~~~~~~~~~~~

1. Checkout master.
2. Decide whether you are releasing a major, minor, or patch revision.
   For assistance in making this choice see the `SemVer`_ standard.
3. Ensure an entry for the version exists in ``docs/history.rst`` summarizing the changes you are releasing.
4. Update the version in docs/conf.py
5. Commit the changes, commenting that you are bumping the version.
6. Tag the repo with the matching version.
7. Push to the central remote and your fork.



 .. |pipeline| image:: https://gitlab.com/ringingmountain/docker/python-http-service/badges/master/pipeline.svg
                :target: https://gitlab.com/ringingmountain/docker/python-http-service/pipelines

.. _Gitlab runner: https://docs.gitlab.com/runner/install/
.. _register: https://docs.gitlab.com/runner/register/index.html#one-line-registration-command
.. _SemVer: https://semver.org
