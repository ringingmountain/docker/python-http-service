FROM python:3.7.2-alpine3.9

LABEL url="https://gitlab.com/ringingmountain/docker/python-http-service" authors="robin@ringingmoutain.com"

COPY requirements.txt .

RUN apk add --no-cache --virtual .build-deps gcc musl-dev && \
    apk add --no-cache postgresql-dev && \
    pip install --no-cache-dir -r requirements.txt && \
    apk --purge del .build-deps
