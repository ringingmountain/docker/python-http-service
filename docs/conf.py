project = 'Python HTTP Service'
copyright = '2019 Ringing Mountain, LLC'
author = 'Robin Klingsberg'

version = '0.1.0'

source_suffix = '.rst'

# The master toctree document.
master_doc = 'index'

htmlhelp_basename = "python_http_service_doc"
