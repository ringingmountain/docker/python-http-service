#!/usr/bin/env sh
set -e

. ci/common.sh

! echo "${CI_REGISTRY_PASSWORD}" | docker login -u "${CI_REGISTRY_USER}" --password-stdin "${CI_REGISTRY}" 2>/dev/null

# Pull image for layer cache, ignore pull failures
! docker pull "${LATEST}" 2>/dev/null

log_run 'Building Docker image' docker build --pull --cache-from "${LATEST}" --tag "${LATEST}" --tag "${TAG}" .
