#!/usr/bin/env sh
set -e

. ci/common.sh

! echo "${CI_REGISTRY_PASSWORD}" | docker login -u "${CI_REGISTRY_USER}" --password-stdin "${CI_REGISTRY}" 2>/dev/null

for VERSION in "${LATEST}" "${TAG}"
do
  log_run "Pushing Docker image ${VERSION}" docker push "${VERSION}"
done
